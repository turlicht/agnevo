# -*- coding: utf-8 -*-

from fabric.api import run, env, cd, lcd, roles, local, settings
from fabric.decorators import hosts

project_path = '/var/www/agnevo/'
touch_path = '/tmp/agnevo.touch'


@hosts('turlicht@178.62.163.239')
def apply(commit_text):
    with settings(warn_only=True):
        local('git add --all')
        if commit_text:
            local('git commit -m"%s"' % commit_text)
        else:
            local('git commit')
        local('git push')

    with cd(project_path):
        run('git pull origin master')
        run('touch ' + touch_path)


def lapply(commit_text=None):
    with settings(warn_only=True):
        local('git add --all')
        if commit_text:
            local('git commit -m"%s"' % commit_text)
        else:
            local('git commit')
        local('git push')

    with lcd(project_path):
        local('git pull origin master')
        local('supervisorctl restart agnevo')
        #local('touch ' + touch_path)
