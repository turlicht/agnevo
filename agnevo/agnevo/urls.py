from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt
from realty.views import SearchView, SubmitView, RealtyView, SubmitSuccessView, SubscribeView, \
    SubscribeDeleteView, RealtyFavouriteView, RevelationView, UserView, UserRealtyDeleteView, UserRealtyShowView, \
    UserRealtyHideView, UserRealtyFavouriteView
from accounts.views import LoginView, LogoutView, RegisterView, ResendView, \
    PayView, PayRobokassaSuccessView, PayRobokassaFailView, \
    PayRobokassaResultView, FeedbackView, FeedbackThanksView, AgreementView, OfferView, DiscountView


admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'AgNeVo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', SearchView.as_view(), name='search'),
    url(r'^submit/$', SubmitView.as_view(), name='submit'),
    url(r'^submit/success/$', SubmitSuccessView.as_view(), name='submit_success'),
    url(r'^realty/(?P<id>\d+)/$', RealtyView.as_view(), name='realty'),
    url(r'^feedback/$', FeedbackView.as_view(), name='feedback'),
    url(r'^feedback/thanks/$', FeedbackThanksView.as_view(), name='feedback_thanks'),
    url(r'^subscribe/$', SubscribeView.as_view(), name='subscribe'),
    url(r'^favourite/$', RealtyFavouriteView.as_view(), name='favourite'),
    url(r'^subscribe/delete/$', SubscribeDeleteView.as_view(), name='subscribe_delete'),
    url(r'^agreement/$', AgreementView.as_view(), name='agreement'),
    url(r'^offer/$', OfferView.as_view(), name='offer'),
    url(r'^discount/$', DiscountView.as_view(), name='discount'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^captcha/', include('captcha.urls')),

    url(r'^account/login/$', LoginView.as_view(), name='login'),
    url(r'^account/log_out/$', LogoutView.as_view(), name='logout'),
    url(r'^account/register/$', RegisterView.as_view(), name='register'),
    url(r'^account/resend/$', ResendView.as_view(), name='resend'),
    url(r'^account/revelation/$', RevelationView.as_view(), name='revelation'),
    url(r'^account/pay/$', PayView.as_view(), name='pay'),
    url(r'^account/pay/robokassa/success/$', PayRobokassaSuccessView.as_view(), name='pay_robokassa_success'),
    url(r'^account/pay/robokassa/fail/$', PayRobokassaFailView.as_view(), name='pay_robokassa_fail'),
    url(r'^account/pay/robokassa/result/$', csrf_exempt(PayRobokassaResultView.as_view()), name='pay_robokassa_result'),
    url(r'^account/user/$', UserView.as_view(), name='user'),
    url(r'^account/user/realty/(?P<pk>\d+)/delete/$', UserRealtyDeleteView.as_view(), name='user_realty_delete'),
    url(r'^account/user/realty/(?P<pk>\d+)/show/$', UserRealtyShowView.as_view(), name='user_realty_show'),
    url(r'^account/user/realty/(?P<pk>\d+)/hide/$', UserRealtyHideView.as_view(), name='user_realty_hide'),
    url(r'^account/user/realty/(?P<pk>\d+)/favourite/$', UserRealtyFavouriteView.as_view(), name='user_realty_favourite'),


    url(r'^api/account/login/$', csrf_exempt(LoginView.as_view())),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
