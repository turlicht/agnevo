# -*- coding: utf-8 -*-
import re
from django import forms
from captcha.fields import CaptchaField


class AccountForm(forms.Form):
    def set_error(self, field, msg):
        self._errors[field] = self.error_class([msg])

    def clean_login(self):
        data = self.cleaned_data['login']
        if not re.match(r'^\d{10}$', data):
            raise forms.ValidationError('Неверный номер телефона')
        return data


class AuthenticationForm(AccountForm):
    login = forms.CharField(label="Phone", max_length=10,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Телефон'}))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}))


class RegistrationForm(AccountForm):
    login = forms.CharField(label="Phone", max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}))
    captcha = CaptchaField()


class ResendForm(AccountForm):
    login = forms.CharField(label="Phone", max_length=10)
    captcha = CaptchaField()


class FeedbackForm(AccountForm):
    name = forms.CharField(label="Name", max_length=255, required=False,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ваше имя'}))
    text = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'текст сообщения', 'rows': '3'}))

