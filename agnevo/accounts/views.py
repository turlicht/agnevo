# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import ListView
from django.views.generic.base import View, RedirectView, TemplateView
from django.views.generic.edit import FormView, DeleteView
from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import utc
from datetime import datetime, timedelta
import hashlib
from accounts.models import User, Revelation, Feedback
from accounts.forms import AuthenticationForm, RegistrationForm, FeedbackForm
from accounts.tools import gen_password, send_sms
from agnevo.settings import ROBOKASSA_PRICES
from common.view import CommonMixin
from content.mixins import ContentMixin
from content.tools import config
from logs.models import Payment, Sms
from agnevo.logging import logger


class LoginView(FormView):
    template_name = 'accounts/login.html'
    form_class = AuthenticationForm
    success_url = '/'

    def get_initial(self):
        number = self.request.GET.get('login', '')
        self.initial['login'] = number
        return self.initial

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['login'],
                            password=form.cleaned_data['password'])
        if user is not None:
            if user.is_active:

                if not user.is_login:
                    user.is_login = True
                    user.save()

                login(self.request, user)
                return super(LoginView, self).form_valid(form)

        form.set_error('login', 'Неравильный логин или пароль')
        return super(LoginView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['login'] = context['form'].data.get('login')
        return context


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class RegisterView(FormView):
    template_name = 'accounts/register.html'
    form_class = RegistrationForm

    def __init__(self, *args, **kwargs):
        self.__error = 'Проверте правильность ввода телефона и текста с картинки'
        super(RegisterView, self).__init__(*args, **kwargs)

    def form_valid(self, form):
        number = form.cleaned_data['login']

        try:
            user = User.objects.get(login=number)
        except ObjectDoesNotExist:
            password = gen_password()
            user = User.objects.create_user(number, config('start_points', 'int', '2'),
                                            password)
            user.sms_datetime = datetime.utcnow().replace(tzinfo=utc)
            user.sms_count += 1
            user.save()
            send_sms(number, 'Пароль для входа: ' + password)
            Sms(user=user, kind='register').save()
            self.success_url = '/account/login?login=' + number
            return super(RegisterView, self).form_valid(form)

        form.set_error('login', 'Пользователь уже существует')
        self.__error = 'Пользователь уже существует'
        return super(RegisterView, self).form_invalid(form)

    def form_invalid(self, form):
        return super(RegisterView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context['error'] = self.__error
        return context


class ResendView(FormView):
    template_name = 'accounts/resend.html'
    form_class = RegistrationForm

    def form_valid(self, form):
        number = form.cleaned_data['login']

        try:
            user = User.objects.get(login=number)
            now = datetime.utcnow().replace(tzinfo=utc)
            if now - user.sms_datetime > timedelta(minutes=5):
                pass
            if True:
                password = gen_password()
                user.set_password(password)
                user.sms_datetime = now
                user.sms_count += 1
                user.save()
                Sms(user=user, kind='resend').save()
                send_sms(number, 'Пароль для входа: ' + password)
                self.success_url = '/account/login?login=' + number
                return super(ResendView, self).form_valid(form)
            else:
                form.set_error('login', 'Превышен интервал отправки, попробуйте через 5 минут')
                return super(ResendView, self).form_invalid(form)
        except ObjectDoesNotExist:
            form.set_error('login', 'Пользователь не существует')
            return super(ResendView, self).form_invalid(form)

    def get_initial(self):
        number = self.request.GET.get('login', '')
        self.initial['login'] = number
        return self.initial


class PayView(CommonMixin, TemplateView):
    template_name = 'accounts/pay.html'

    def get_context_data(self, **kwargs):
        context = super(PayView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            return context

        user_id = self.request.user.id

        for sum, period in ROBOKASSA_PRICES.items():
            description = 'Оплата для просмотра объявлений, период: %s дней' % period
            crc = '%s:%s:%s:%s' % (settings.ROBOKASSA_LOGIN, sum, user_id, settings.ROBOKASSA_PASS1)
            crc = hashlib.md5(crc.encode()).hexdigest()
            context['robokassa_url_' + str(period)] = (
                'https://auth.robokassa.ru/Merchant/PaymentForm/FormMS.js' +
                '?MerchantLogin=%s&OutSum=%s&InvoiceID=%s&Description=%s&Culture=ru&Encoding=utf-8&SignatureValue=%s'
                % (settings.ROBOKASSA_LOGIN, sum, user_id, description, crc)
            )
            context['robokassa_price_' + str(period)] = sum
        return context


class PayRobokassaSuccessView(CommonMixin, TemplateView):
    template_name = 'accounts/pay_robokassa_success.html'


class PayRobokassaFailView(CommonMixin, TemplateView):
    template_name = 'accounts/pay_robokassa_fail.html'


class PayRobokassaResultView(CommonMixin, View):

    def post(self, request):
        out_sum = self.request.POST.get('OutSum', None)
        inv_id = self.request.POST.get('InvId', None)
        crc = self.request.POST.get('SignatureValue', None)
        logger('ROBOKASSA POST: %s', str(self.request.POST))
        logger('ROBOKASSA OutSum:%s InvId:%s SignatureValue:%s', out_sum, inv_id, crc)

        if not out_sum or not inv_id or not crc:
            logger('ROBOKASSA InvId:%s ERROR: params not found', inv_id)
            return HttpResponse('ERROR: params not found')

        try:
            user = User.objects.get(id=int(inv_id))
        except ObjectDoesNotExist:
            logger('ROBOKASSA InvId:%s ERROR: user not found', inv_id)
            return HttpResponse('ERROR: user not found')

        my_crc = "%s:%s:%s" % (out_sum, inv_id, settings.ROBOKASSA_PASS2)
        my_crc = hashlib.md5(my_crc.encode()).hexdigest()
        logger('ROBOKASSA InvId:%s CALC CRC: %s', inv_id, my_crc)

        if not my_crc.upper() == crc.upper():
            logger('ROBOKASSA InvId:%s ERROR: crc not match', inv_id)
            return HttpResponse('ERROR: crc not match')

        out_sum = int(float(out_sum))
        Payment(user=user, sum=out_sum).save()

        if out_sum in ROBOKASSA_PRICES:
            delta = timedelta(days=ROBOKASSA_PRICES[out_sum])
        else:
            delta = timedelta(days=out_sum/settings.ROBOKASSA_PRICE_COEFF)
        user.premium_to = datetime.utcnow().replace(tzinfo=utc) + delta
        user.save()

        return HttpResponse('OK' + str(inv_id))


class FeedbackView(CommonMixin, ContentMixin, FormView):
    template_name = 'accounts/feedback.html'
    form_class = FeedbackForm
    success_url = '/feedback/thanks/'
    content_names = ['about']

    def form_valid(self, form):
        feedback = Feedback(name=form.cleaned_data['name'], text=form.cleaned_data['text'])
        if self.request.user.is_authenticated():
            feedback.user = self.request.user
        feedback.save()
        email_message = 'New feedback message: http://xn--80aeci3bi.xn--p1ai/admin/accounts/feedback/'
        send_mail('Agnevo Feedback', email_message,
                  settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER],
                  fail_silently=True)
        return super(FeedbackView, self).form_valid(form)


class FeedbackThanksView(CommonMixin, TemplateView):
    template_name = 'accounts/feedback_thanks.html'


class AgreementView(CommonMixin, ContentMixin, TemplateView):
    template_name = 'accounts/agreement.html'
    content_names = ['agreement']


class OfferView(CommonMixin, ContentMixin, TemplateView):
    template_name = 'accounts/offer.html'
    content_names = ['offer']


class DiscountView(CommonMixin, ContentMixin, TemplateView):
    template_name = 'accounts/discount.html'
    content_names = ['discount']