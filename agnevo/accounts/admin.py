from django import forms
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from accounts.models import User, Revelation, Feedback, Favourite, Subscribe


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('login', 'points')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['login', 'password', 'points', 'is_active', 'is_admin']

    def clean_password(self):
        return self.initial["password"]


class UserAdmin(DjangoUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    fieldsets = (
        (None, {'fields': ('login', 'password')}),
        ('Bonuses', {'fields': ('points', 'premium_to',)}),
        ('Permissions', {'fields': ('is_active', 'is_admin', 'is_agent',)}),
        ('Sms', {'fields': ('sms_datetime', 'sms_count',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('login', 'points',  'is_admin', 'is_agent', 'password1', 'password2'),
        }
        ),
    )

    list_display = ('login', 'points', 'premium_to', 'is_agent', 'is_admin', 'is_login', 'created', 'last_login')
    list_filter = ('is_admin', 'is_login', 'is_agent')
    search_fields = ('login',)
    ordering = ('-created',)
    filter_horizontal = ()


class FeedbackAdmin(ModelAdmin):
    list_display = ('date', 'user', 'name', 'text')


class FavouriteAdmin(ModelAdmin):
    list_display = ('user', 'realty')


class RevelationAdmin(ModelAdmin):
    list_display = ('date_access', 'user', 'realty')
    ordering = ('-date_access',)


class SubscribeAdmin(ModelAdmin):
    list_display = ('user', 'city', 'district', 'kind', 'price_from', 'price_to', 'furniture', 'duration')


admin.site.register(User, UserAdmin)
admin.site.register(Revelation, RevelationAdmin)
admin.site.register(Favourite, FavouriteAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Subscribe, SubscribeAdmin)
admin.site.unregister(Group)