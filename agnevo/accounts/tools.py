import urllib
from urllib.request import HTTPError
from base64 import b64encode
from agnevo.logging import logger
from agnevo.settings import SMSFEEDBACK_LOGIN, SMSFEEDBACK_PASSWORD
from random import randrange


def gen_password():
    return str(randrange(100000, 999999))


def send_sms(number, text):
    try:
        opener = urllib.request.build_opener()
        auth = SMSFEEDBACK_LOGIN + ':' + SMSFEEDBACK_PASSWORD
        auth = b64encode(auth.encode())
        opener.addheaders = [
            ('Host', 'api.smsfeedback.ru'),
            ('Authorization', 'Basic ' + auth.decode()),
        ]
        url = "http://api.smsfeedback.ru/messages/v2/send/?phone=7%s&text=%s" % (number, urllib.parse.quote(text))
        response = opener.open(url)
        logger.info('Send SMS: %s', response.msg)
    except HTTPError as e:
        logger.error('Error send SMS: %s %s %s', number, text, e)
