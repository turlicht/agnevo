# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.utils.timezone import utc
from datetime import datetime
from realty.models import Realty


class UserManager(BaseUserManager):
    def create_user(self, login, points, password=None):
        if not login:
            raise ValueError('Users must have an login')

        user = self.model(login=login)
        user.set_password(password)
        user.points = points
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password):
        user = self.create_user(login, 0, password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    login = models.CharField(verbose_name='Логин', max_length=255, unique=True, db_index=True)

    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    points = models.PositiveIntegerField(verbose_name='Баллы', default=0)
    premium_to = models.DateTimeField(verbose_name='Действие премиума до', blank=True, null=True)
    realties = models.ManyToManyField(Realty, through='Revelation', related_name='realities_revelation')
    favourites = models.ManyToManyField(Realty, through='Favourite', related_name='realities_favourite')

    sms_datetime = models.DateTimeField(null=True)
    sms_count = models.PositiveIntegerField(default=0)
    is_login = models.BooleanField(verbose_name='Вход', default=False)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    is_admin = models.BooleanField(verbose_name='Админ', default=False)
    is_agent = models.BooleanField(verbose_name='Агент', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return self.login

    def get_short_name(self):
        return self.login

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def __str__(self):
        return self.login

    def is_premium(self):
        now = datetime.utcnow().replace(tzinfo=utc)
        if self.premium_to and self.premium_to > now:
            return True
        return False


class Revelation(models.Model):
    user = models.ForeignKey(User)
    realty = models.ForeignKey(Realty)
    date_access = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.login + ' - ' + self.realty.address()


class Favourite(models.Model):
    user = models.ForeignKey(User)
    realty = models.ForeignKey(Realty)

    def __str__(self):
        return self.user.login + ' - ' + self.realty.address()


class Feedback(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.date) + ' - ' + self.text


class Subscribe(models.Model):
    user = models.ForeignKey('accounts.User')
    city = models.TextField(blank=True, null=True, default=None)
    district = models.TextField(blank=True, null=True, default=None)
    kind = models.TextField(blank=True, null=True, default=None)
    price_from = models.PositiveIntegerField(null=True, default=None)
    price_to = models.PositiveIntegerField(null=True, default=None)
    furniture = models.CharField(max_length=100, blank=True, null=True, default=None)
    duration = models.CharField(max_length=100, blank=True, null=True, default=None)

    def __str__(self):
        return self.user.login
