# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from accounts.forms import AuthenticationForm
from content.models import Content


class CommonMixin(object):

    require_auth = False

    def dispatch(self, request, *args, **kwargs):
        if self.require_auth and not request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return super(CommonMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CommonMixin, self).get_context_data(**kwargs)
        context['cur_path'] = self.request.get_full_path()
        context['auth_form'] = AuthenticationForm()
        context['menu_selected'] = {
            self.request.resolver_match.url_name: 'active'
        }

        content, _ = Content.objects.get_or_create(kind='discount', defaults={'is_active': False})
        context['is_discount'] = content.is_active

        return context
