from django.contrib.admin import ModelAdmin
from django.contrib import admin
from logs.models import Payment, Sms


class LogAdmin(ModelAdmin):
    fieldsets = (
        (None, {'fields': ()}),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    # def get_form(self, request, obj=None, **kwargs):
    #     form = super(PaymentAdmin, self).get_form(request, obj, **kwargs)
    #     form.base_fields['password'] = {'initial': '123'}
    #     return form


class PaymentAdmin(LogAdmin):
    list_display = ('date', 'user', 'sum')
    ordering = ('-date',)


class SmsAdmin(LogAdmin):
    list_display = ('date', 'user', 'kind')
    ordering = ('-date',)


# class PointAdmin(LogAdmin):
#     pass


admin.site.register(Payment, PaymentAdmin)
admin.site.register(Sms, SmsAdmin)