from django.db import models
from accounts.models import User
from realty.models import Realty


class Payment(models.Model):
    user = models.ForeignKey(User)
    sum = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.date)


class Sms(models.Model):
    SMS_KIND = (
        ('register', 'регистрация'),
        ('resend', 'повторное получение пароля'),
        ('subscribe', 'рассылка по подписке'),
    )

    kind = models.CharField(max_length=100, choices=SMS_KIND, verbose_name="тип")
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.date)

# ---
# class Point(models.Model):
#     user = models.ForeignKey(User)
#     date = models.DateTimeField(auto_now_add=True)
#     realty = models.ForeignKey(Realty)
#     action = models.CharField(max_length=255, blank=True)
#
#     def __unicode__(self):
#         return str(self.date)