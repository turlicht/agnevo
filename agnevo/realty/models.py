# -*- coding: utf-8 -*-
from django.db import models
from realty.tools import get_yandex_object


class Region(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class City(models.Model):
    region = models.ForeignKey('Region')
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.region.name + ' - ' + self.name


class District(models.Model):
    city = models.ForeignKey('City')
    name = models.CharField(max_length=100)

    def short_name(self):
        return self.name.split()[0]

    def __str__(self):
        return self.city.name + ' - ' + self.name


class Realty(models.Model):
    REALTY_KIND = [
        ('apartment_1', u'1 комнатная квартира'),
        ('apartment_2', u'2-х комнатная квартира'),
        ('apartment_3', u'3-х комнатная квартира'),
        ('apartment_4', u'4 и более комнат'),
        ('studio', u'квартира студия'),
        ('room', u'комната'),
        ('dormitory', u'общежитие'),
        ('homestead', u'частный дом'),
        ('homestead_part', u'часть дома'),
        ('bed', u'койко-место'),
        ('garage', u'гараж'),
        # ('shop', u'торговое помещение'),
        # ('office', u'офисное помещение'),
        # ('store', u'складское помещение'),
        # ('production', u'производственное помещение'),
    ]

    DURATION_TYPE = (
        ('long', 'длительный период'),
        ('daily', 'посуточно'),
    )

    ADVERTISEMENT_TYPE = (
        ('lease', 'Сдаю'),
        ('sell', 'Продаю'),
    )

    city = models.ForeignKey('City', verbose_name="город")
    district = models.ForeignKey('District', blank=True, null=True, verbose_name="район")
    sub_district = models.CharField(max_length=100, blank=True, null=True, verbose_name="поселок или микрорайон")
    street = models.CharField(max_length=100, verbose_name="улица")
    house = models.CharField(max_length=100, verbose_name="номер дома")
    lat = models.FloatField()
    lon = models.FloatField()

    price = models.PositiveIntegerField(verbose_name="цена")
    contact = models.CharField(max_length=100, verbose_name="контактный телефон")

    kind = models.CharField(max_length=100, choices=REALTY_KIND, verbose_name="тип")
    duration = models.CharField(max_length=100, choices=DURATION_TYPE, verbose_name="длительность")
    is_furniture = models.BooleanField(default=False, verbose_name="есть ли мебель")
    floor = models.PositiveIntegerField(verbose_name="этаж")
    storeys = models.PositiveIntegerField(verbose_name="количество этажей в доме")
    description = models.TextField(blank=True, verbose_name="описание")

    image1 = models.ImageField(blank=True, verbose_name="Изображение", upload_to='images', default=None, null=True)
    image2 = models.ImageField(blank=True, verbose_name="Изображение", upload_to='images', default=None, null=True)
    image3 = models.ImageField(blank=True, verbose_name="Изображение", upload_to='images', default=None, null=True)
    image4 = models.ImageField(blank=True, verbose_name="Изображение", upload_to='images', default=None, null=True)
    image5 = models.ImageField(blank=True, verbose_name="Изображение", upload_to='images', default=None, null=True)

    is_show = models.BooleanField(default=True, verbose_name="показать")
    is_publish = models.BooleanField(default=False, verbose_name="опубликовано")
    submit_date = models.DateTimeField()
    publish_date = models.DateTimeField(blank=True, null=True)
    check_date = models.DateTimeField(blank=True, null=True)

    user = models.ForeignKey('accounts.User', blank=True, null=True, related_name='+')

    def __str__(self):
        return self.city.name + ' - ' + self.street + ' ' + self.house

    def address(self):
        return self.street + ' ' + self.house

    def kind_display(self):
        for choise in self.REALTY_KIND:
            if self.kind == choise[0]:
                return choise[1]

    def is_furniture_display(self):
        if self.is_furniture:
            return 'да'
        return 'нет'

    def check_date_display(self):
        if self.check_date:
            return self.check_date
        return 'нет'

    def lat_display(self):
        return str(self.lat)

    def lon_display(self):
        return str(self.lon)

    def save(self, *args, **kwargs):
        if not self.lat and not self.lon:
            obj = get_yandex_object('Россия', self.city.region.name, self.city.name,
                                    self.street, self.house)
            if obj:
                self.lat = obj['lat']
                self.lon = obj['lon']

        super(Realty, self).save(*args, **kwargs)

        # if self.user.is_admin and self.is_publish and self.is_show:
        #     print('Send sms')
        #     delivery_realities_sms(self.id)

