import urllib
from urllib.parse import quote, unquote
import json
from time import sleep
from agnevo.logging import logger


def create_query(country, region, locality, street, house, encode):
    if not region:
        region = ''
    if not locality:
        locality = ''
    if not street:
        street = ''
    if not house:
        house = ''

    query = country + ', ' + region + ', ' + locality + ', ' + street + ', ' + house
    if encode:
        query = quote(query.encode(encode))
    else:
        query = quote(query)

    return query


def get_yandex_object(country, region, locality='', street='', house='', encode='utf-8'):
    query = create_query(country, region, locality, street, house, encode)
    url = 'http://geocode-maps.yandex.ru/1.x/?geocode=' + query + '&format=json'
    try:
        f = urllib.request.urlopen(url)
        data = f.read().decode()
    except IOError:
        logger.error('Yandex IO Error, url: %s' % url)
        return

    try:
        response = json.loads(data)

        geoObject = response['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']
        result = dict()
        kind = geoObject['metaDataProperty']['GeocoderMetaData']['kind']
        dependent = None
        if kind == 'locality':
            try:
                dependent = geoObject['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['DependentLocality']
            except NameError:
                dependent = None
            except TypeError:
                dependent = None
            except KeyError:
                dependent = None

        if dependent:
            kind = 'locality_dependent'

        result['yandex_url'] = url
        result['kind'] = kind
        result['text'] = geoObject['metaDataProperty']['GeocoderMetaData']['text']
        pos = geoObject['Point']['pos']
        result['lon'] = float(pos.split()[0])
        result['lat'] = float(pos.split()[1])
        return result
    except Exception as e:
        logger.error('Yandex parse Error: %s' % data)
        return None


def get_google_object(country, region, locality='', street='', house='', encode='utf-8'):
    query = create_query(country, region, locality, street, house, encode)
    url = ('http://maps.googleapis.com/maps/api/geocode/json?address=' + query +
           '&sensor=false&language=ru')
    try:
        f = urllib.request.urlopen(url)
        data = f.read()
    except IOError as e:
        print('Google IO Error!')
        print(url)
        print(e.message)
        sleep(60)
        return None
    except Exception as e:
        print('Google other Error!')
        print(e.message)
        return None

    try:
        data = json.loads(data)
        if len(data['results']) < 1:
            return None

        geoObject = data['results'][0]
        result = dict()
        result['kind'] = geoObject['types'][0]
        text = geoObject['formatted_address']
        text_split = text.split(',')
        text = ''
        for i in reversed(range(len(text_split))):
            if i == 0:
                text += text_split[i].strip()
            else:
                text += text_split[i].strip() + ', '

        result['text'] = text
        result['lat'] = float(geoObject['geometry']['location']['lat'])
        result['lon'] = float(geoObject['geometry']['location']['lng'])
        return result
    except Exception as e:
        print('Google parse Error!')
        print(e.message)
        return None