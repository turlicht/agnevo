from django.contrib import admin
from django.contrib.admin import ModelAdmin
from realty.models import Region, City, District, Realty
from realty.tasks import delivery_realities_sms


class RealtyAdmin(ModelAdmin):
    class Media:
        js = ('http://api-maps.yandex.ru/2.1/?lang=ru_RU', )

    change_form_template = 'admin/realty/ymaps_change_form.html'

    list_display = ('realty_full_address', 'user', 'is_publish', 'is_show', 'submit_date',
                    'publish_date', 'check_date',)
    list_filter = ('is_publish', 'is_show',)
    search_fields = ('street',)
    ordering = ('-check_date', )

    def realty_full_address(self, obj):
        return obj
    realty_full_address.short_description = 'Realty'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        #extra_context['y_maps'] = self.y_maps()
        return super(RealtyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.is_publish and obj.is_show:
            delivery_realities_sms(obj.id)


admin.site.register(Region)
admin.site.register(City)
admin.site.register(District)
admin.site.register(Realty, RealtyAdmin)