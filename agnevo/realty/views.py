# -*- coding: utf-8 -*-
from collections import defaultdict
import json
from accounts.models import Favourite, Revelation, Subscribe
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, DeleteView, RedirectView, View
from django.views.generic.edit import FormView
from django.utils.timezone import utc
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponseRedirect
from datetime import datetime, timedelta

from common.view import CommonMixin
from realty.models import Realty, City, District
from realty.forms import SearchForm, SubmitForm
from utils.filters_form_mixin import FiltersFormMixin


def get_access_contact(user, realty):
    if user.is_authenticated():
        now = datetime.utcnow().replace(tzinfo=utc)
        try:
            revelation = user.revelation_set.get(realty=realty)
        except ObjectDoesNotExist:
            revelation = None

        if user.premium_to and user.premium_to > now:
            access = 'premium'
        elif revelation:
            access = 'revelation'
        elif user.points > 0:
            access = 'points'
        else:
            access = 'none'
    else:
        access = 'no_auth'

    return access


def get_district_data(initial=None):
    if not initial:
        initial = {}
    districts = District.objects.all()
    district_data = defaultdict(str)
    for district in districts:
        selected = ''
        if str(district.id) in initial:
            selected = 'selected="selected"'
        district_data[str(district.city.id)] += \
            '<option value="%s"%s>%s</option>' % (district.id, selected, district.name)
    return json.dumps(district_data)


def save_subscribe(user, form):
    Subscribe.objects.filter(user=user).delete()
    subscribe = Subscribe(user=user)
    if form.cleaned_data['price_from']:
        subscribe.price_from = int(form.cleaned_data['price_from'])
    if form.cleaned_data['price_to']:
        subscribe.price_to = int(form.cleaned_data['price_to'])
    if form.cleaned_data['duration']:
        subscribe.duration = form.cleaned_data['duration']
    if form.cleaned_data['is_furniture']:
        subscribe.furniture = form.cleaned_data['is_furniture']
    if form.cleaned_data['city']:
        subscribe.city = str(form.cleaned_data['city'].id)
    if form.cleaned_data['district']:
        s = '|'
        for district in form.cleaned_data['district']:
            s += str(district.id) + '|'
        subscribe.district = s

    if form.cleaned_data['kind']:
        s = '|'
        for kind in form.cleaned_data['kind']:
            s += kind + '|'
        subscribe.kind = s
    subscribe.save()


class RealtyView(CommonMixin, TemplateView):
    template_name = 'realty/realty.html'

    def get_context_data(self, **kwargs):
        context = super(RealtyView, self).get_context_data(**kwargs)
        try:
            realty = Realty.objects.get(id=int(kwargs['id']))
            context['realty'] = realty
        except ObjectDoesNotExist:
            raise Http404

        context['realty'].access_contact = get_access_contact(self.request.user,
                                                              context['realty'])
        context['images'] = []
        for i in range(1, 6):
            image = getattr(realty, 'image' + str(i))
            if image:
                context['images'].append(image.url)
        return context


class SearchView(CommonMixin, FiltersFormMixin, ListView):
    template_name = 'realty/search.html'
    success_url = '/'
    context_object_name = 'realties'
    paginate_by = 20
    form_class = SearchForm

    def get_queryset(self):
        is_favourite = True if self.request.COOKIES.get('is_favourite', False) else False

        filters = self.get_filters()
        realties = Realty.objects.filter(is_publish=True, is_show=True)
        if 'city' in filters:
            realties = realties.filter(city=int(filters['city']))
        else:
            volgograg = City.objects.get(name=u'Волгоград')
            realties = realties.filter(city=volgograg)

        if 'district' in filters:
            realties = realties.filter(district__in=filters['district'])
        if 'kind'in filters:
            realties = realties.filter(kind__in=filters['kind'])
        if 'is_furniture' in filters:
            realties = realties.filter(is_furniture=True if filters['is_furniture'] == 'yes' else False)
        if 'price_from'in filters:
            realties = realties.filter(price__gte=filters['price_from'])
        if 'price_to'in filters:
            realties = realties.filter(price__lte=filters['price_to'])
        if 'duration'in filters:
            realties = realties.filter(duration=filters['duration'])
        if 'is_photo' in filters:
            realties = realties.exclude(image1="")

        if self.request.user.is_authenticated() and is_favourite:
            realties = realties.filter(id__in=self.request.user.favourites.all())

        return realties.order_by('-check_date')

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        for realty in context['realties']:
            realty.access_contact = get_access_contact(self.request.user, realty)

            realty.is_favourite = False
            if self.request.user.is_authenticated() and self.request.user in realty.realities_favourite.all():
                realty.is_favourite = True
        context['district_data'] = get_district_data(context['form'].initial.get('district', []))
        context['is_favourite'] = True if self.request.COOKIES.get('is_favourite', False) else False
        return context


class UserView(CommonMixin, ListView):
    template_name = 'realty/user.html'
    context_object_name = 'realties'
    paginate_by = 20
    require_auth = True

    def get_queryset(self):
        return Realty.objects.filter(user=self.request.user).order_by('-submit_date')


class UserRealtyDeleteView(DeleteView):
    model = Realty
    template_name = 'realty/user_realty_delete.html'
    success_url = reverse_lazy('user')


class RealtyFavouriteView(RedirectView):
    url = reverse_lazy('search')
    permanent = False

    def get(self, request, *args, **kwargs):
        response = super(RealtyFavouriteView, self).get(request, *args, **kwargs)
        if request.COOKIES.get('is_favourite', False):
            response.set_cookie('is_favourite', '')
        else:
            response.set_cookie('is_favourite', '1')
        return response


class UserRealtyHideView(RedirectView):
    url = reverse_lazy('user')
    permanent = False

    def get(self, request, *args, **kwargs):
        realty = Realty.objects.get(pk=kwargs['pk'])
        realty.is_show = False
        realty.save()
        return super(UserRealtyHideView, self).get(request, *args, **kwargs)


class UserRealtyShowView(RedirectView):
    url = reverse_lazy('user')
    permanent = False

    def get(self, request, *args, **kwargs):
        realty = Realty.objects.get(pk=kwargs['pk'])
        realty.is_show = True
        realty.save()
        return super(UserRealtyShowView, self).get(request, *args, **kwargs)


class UserRealtyFavouriteView(RedirectView):
    url = reverse_lazy('search')
    permanent = False

    def get(self, request, *args, **kwargs):
        realty = Realty.objects.get(pk=kwargs['pk'])
        try:
            favourite = Favourite.objects.get(user=request.user, realty=realty)
            favourite.delete()
        except Favourite.DoesNotExist:
            Favourite(user=request.user, realty=realty).save()
        return super(UserRealtyFavouriteView, self).get(request, *args, **kwargs)


class SubscribeView(CommonMixin, FiltersFormMixin, TemplateView):
    template_name = 'realty/subscribe.html'
    form_class = SearchForm

    def form_valid(self, form):
        save_subscribe(self.request.user, form)
        return HttpResponseRedirect('/subscribe/?alert=subscribe')

    def get_initial(self):
        return {}

    def get_context_data(self, **kwargs):
        context = super(SubscribeView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            return context

        try:
            context['subscribe'] = Subscribe.objects.get(user=self.request.user)
            is_all = True
            if context['subscribe'].furniture:
                is_all = False
                if context['subscribe'].furniture == 'yes':
                    context['subscribe'].furniture = 'да'
                elif context['subscribe'].furniture == 'no':
                    context['subscribe'].furniture = 'нет'

            if context['subscribe'].duration:
                is_all = False
                for type, name in Realty.DURATION_TYPE:
                    if type == context['subscribe'].duration:
                        context['subscribe'].duration = name

            if context['subscribe'].kind:
                is_all = False
                result = []
                for kind in context['subscribe'].kind.split('|'):
                    for type, name in Realty.REALTY_KIND:
                        if type == kind:
                            result.append(name)
                context['subscribe'].kind = ', '.join(result)

            if context['subscribe'].city:
                is_all = False
                context['subscribe'].city = City.objects.get(id=context['subscribe'].city).name

            if context['subscribe'].district:
                is_all = False
                result = []
                for district_id in context['subscribe'].district.split('|'):
                    if not district_id:
                        continue

                    district = District.objects.get(id=int(district_id))
                    result.append(district.name)
                context['subscribe'].district = ', '.join(result)

            context['subscribe'].is_all = is_all
        except ObjectDoesNotExist:
            context['subscribe'] = None

        context['alert'] = self.request.GET.get('alert', None)
        context['district_data'] = get_district_data()
        return context


class SubscribeDeleteView(CommonMixin, TemplateView):
    template_name = 'realty/subscribe_delete.html'

    def post(self, request):
        Subscribe.objects.filter(user=self.request.user).delete()
        return HttpResponseRedirect('/subscribe/?alert=delete_subscribe')


class SubmitView(CommonMixin, FormView):
    template_name = 'realty/submit.html'
    success_url = '/submit/success/'
    form_class = SubmitForm

    def form_valid(self, form):
        now = datetime.utcnow().replace(tzinfo=utc)
        realty = Realty()
        realty.city = form.cleaned_data['city']
        realty.district = form.cleaned_data['district']
        #realty.city = realty.district.city
        realty.sub_district = form.cleaned_data['sub_district']
        realty.street = form.cleaned_data['street']
        realty.house = form.cleaned_data['house']
        realty.lat = form.cleaned_data['lat']
        realty.lon = form.cleaned_data['lon']
        realty.price = form.cleaned_data['price']
        realty.contact = form.cleaned_data['contact']
        realty.kind = form.cleaned_data['kind']
        realty.duration = form.cleaned_data['duration']
        realty.is_furniture = form.cleaned_data['is_furniture']
        realty.floor = form.cleaned_data['floor']
        realty.storeys = form.cleaned_data['storeys']
        realty.description = form.cleaned_data['description']
        realty.image1 = form.cleaned_data['image1']
        realty.image2 = form.cleaned_data['image2']
        realty.image3 = form.cleaned_data['image3']

        realty.submit_date = now
        # if self.request.user.is_authenticated():
        #     if self.request.user.is_admin:
        #         realty.is_publish = True
        #         realty.publish_date = now
        #         realty.check_date = now

        if self.request.user.is_authenticated():
            realty.user = self.request.user

        realty.save()

        return super(SubmitView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SubmitView, self).get_context_data(**kwargs)
        context['district_data'] = get_district_data()
        return context


class SubmitSuccessView(CommonMixin, TemplateView):
    template_name = 'realty/submit_success.html'


class RevelationView(View):

    def post(self, request, *args, **kwargs):
        redirect_url = request.POST.get('return_path', '/')
        user = self.request.user

        try:
            realty_id = request.POST.get('realty_id', None)
            realty = Realty.objects.get(id=int(realty_id))
        except ObjectDoesNotExist:
            realty = None

        try:
            revelation = user.revelation_set.get(realty=realty)
        except ObjectDoesNotExist:
            revelation = None

        if realty and user.points > 0 and not revelation:
            Revelation.objects.create(user=user, realty=realty)
            user.points -= 1
            user.save()

        return HttpResponseRedirect(redirect_url)
