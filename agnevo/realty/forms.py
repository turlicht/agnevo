from django import forms
from django.forms import Form, ModelForm
from django.forms import ModelChoiceField
from captcha.fields import CaptchaField
from realty.models import Realty, City, District


class BootstrapSelect(forms.Select):
    def __init__(self, attrs=None):
        result_attrs = {'class': 'form-control'}
        if attrs:
            result_attrs.update(attrs)
        super(BootstrapSelect, self).__init__(attrs=result_attrs)


class CityField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class DistrictField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj


class DistrictMultiField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name

    def __instancecheck__(self, instance):
        if instance is forms.ModelMultipleChoiceField:
            return True


class SearchForm(Form):
    # KINDS = [('', 'тип...')] + Realty.REALTY_KIND
    KINDS = Realty.REALTY_KIND
    PRICES_FROM = (
        ('', 'цена от...'),
        ('3000', '3000'),
        ('5000', '5000'),
        ('7000', '7000'),
        ('10000', '10000'),
        ('12000', '12000'),
        ('15000', '15000'),
        ('20000', '20000'),
    )
    PRICES_TO = (
        ('', 'цена до...'),
        ('3000', '3000'),
        ('5000', '5000'),
        ('7000', '7000'),
        ('10000', '10000'),
        ('12000', '12000'),
        ('15000', '15000'),
        ('20000', '20000'),
    )
    FURNITURE = (
        ('', 'мебель...'),
        ('yes', 'с мебелью'),
        ('no', 'без мебели'),
    )
    DURATION = (
        ('', 'период...'),
        ('long', 'длительный'),
        ('daily', 'посуточно'),
    )

    city = CityField(queryset=City.objects, required=True, label=None, empty_label=None,
                     widget=forms.Select(attrs={'class': 'form-control'}))
    district = DistrictMultiField(queryset=District.objects, required=False)
    kind = forms.MultipleChoiceField(choices=KINDS, required=False)
    is_photo = forms.BooleanField(required=False, help_text='С фото')

    # city = CityField(queryset=City.objects, empty_label='город...', required=False, initial=1,
    #                  widget=forms.Select(attrs={'class': 'form-control'}))
    # district = DistrictField(queryset=District.objects, empty_label='район...', required=False,
    #                          widget=forms.Select(attrs={'class': 'form-control'}))
    # kind = forms.ChoiceField(choices=KINDS, required=False,
    #                          widget=forms.Select(attrs={'class': 'form-control'}))
    is_furniture = forms.ChoiceField(choices=FURNITURE, required=False,
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    price_from = forms.ChoiceField(choices=PRICES_FROM, required=False,
                                   widget=forms.Select(attrs={'class': 'form-control'}))
    price_to = forms.ChoiceField(choices=PRICES_TO, required=False,
                                 widget=forms.Select(attrs={'class': 'form-control'}))
    duration = forms.ChoiceField(choices=DURATION, required=False,
                                 widget=forms.Select(attrs={'class': 'form-control'}))


class SubmitForm(Form):
    city = CityField(queryset=City.objects.all(), empty_label=None,
                     widget=forms.Select(attrs={'class': 'form-control'}))
    district = DistrictField(queryset=District.objects, label='Район',
                             empty_label=None, required=False,
                             widget=forms.Select(attrs={'class': 'form-control'}))
    sub_district = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'необязательное поле'}))
    street = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    house = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    lat = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    lon = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    duration = forms.ChoiceField(choices=Realty.DURATION_TYPE, widget=BootstrapSelect)
    price = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'за месяц или за сутки если посуточно'}))
    contact = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'телефон и имя'}))

    kind = forms.ChoiceField(choices=Realty.REALTY_KIND, widget=BootstrapSelect)

    is_furniture = forms.BooleanField(required=False)

    floor = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    storeys = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '3'}))

    image1 = forms.ImageField(required=False)
    image2 = forms.ImageField(required=False)
    image3 = forms.ImageField(required=False)
    image4 = forms.ImageField(required=False)
    image5 = forms.ImageField(required=False)

    captcha = CaptchaField(label='Введите текст с картинки')

