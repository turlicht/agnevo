# -*- coding: utf-8 -*-
from django.db.models import Q
from accounts.tools import send_sms
from logs.models import Sms


def delivery_realities_sms(realty_id):
    from accounts.models import Subscribe
    from realty.models import Realty
    realty = Realty.objects.get(id=realty_id)
    if realty.is_furniture:
        subs_furniture = 'yes'
    else:
        subs_furniture = 'no'
    subscribes = Subscribe.objects
    if realty.district:
        subscribes = subscribes.filter(Q(district__isnull=True) | Q(district__contains='|%s|' % realty.district.id))
    subscribes = subscribes.filter(Q(city__isnull=True) | Q(city='%s' % realty.city.id))
    subscribes = subscribes.filter(Q(kind__isnull=True) | Q(kind__contains='|%s|' % realty.kind))
    subscribes = subscribes.filter(Q(furniture__isnull=True) | Q(furniture=subs_furniture))
    subscribes = subscribes.filter(Q(duration__isnull=True) | Q(duration=realty.duration))
    subscribes = subscribes.filter(Q(price_from__isnull=True) | Q(price_from__lte=realty.price))
    subscribes = subscribes.filter(Q(price_to__isnull=True) | Q(price_to__gte=realty.price))

    info = realty.kind_display() + '. ' + realty.city.name + ', '
    if realty.district:
        info += realty.district.short_name() + ' район,'
    if realty.sub_district:
        info += realty.sub_district + ', '
    info += realty.address() + '. '
    info += realty.contact + '. '
    info += 'Цена: ' + str(realty.price)

    #print subscribes.query

    for subscribe in subscribes:
        if subscribe.user.is_premium():
            send_sms(subscribe.user.login, info)
            Sms(user=subscribe.user, kind='resend').save()
