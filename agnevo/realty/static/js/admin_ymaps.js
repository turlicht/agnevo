ymaps.ready(init);
var map, placemark;

function init(){
    map = new ymaps.Map ("map", {
        center: [48.710352268424195, 44.51955409023755],
        zoom: 10
    });

    var marker = new ymaps.GeoObject({
        geometry: {
            type: "Point",
            coordinates: [48.710352268424195, 44.51955409023755]
        },
        properties: {
            iconContent: 'Укажите дом',
            hintContent: 'Перетащите элемент'
        }
    },
    {
        preset: 'islands#blackStretchyIcon',
        draggable: true
    });

    marker.events.add('dragend', function (e) {
        var target = e.get('target');
        var coords = target.geometry.getCoordinates();
        alert(coords.join(', '));
    });
    map.geoObjects.add(marker);
}