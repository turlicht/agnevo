from django.contrib.admin import ModelAdmin
from django.contrib import admin
from content.models import Content, Config


class ContentAdmin(ModelAdmin):
    list_display = ('kind', 'is_active')


class ConfigAdmin(ModelAdmin):
    list_display = ('name', 'kind', 'value')


admin.site.register(Config, ConfigAdmin)
admin.site.register(Content, ContentAdmin)