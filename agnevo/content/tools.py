from content.models import Config


def config(name, default_kind='str', default_value=''):
    conf, _ = Config.objects.get_or_create(name=name, defaults={'kind': default_kind, 'value': default_value})
    if conf.kind == 'int':
        return int(conf.value)
    if conf.kind == 'str':
        return str(conf.value)
