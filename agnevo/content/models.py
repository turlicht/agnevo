from django.db import models


class Content(models.Model):
    CONTENT_KIND = (
        ('about', 'о проекте'),
        ('agreement', 'пользовательское соглашение'),
        ('offer', 'оферта'),
        ('discount', 'акция'),
    )

    kind = models.CharField(max_length=100, choices=CONTENT_KIND, unique=True, verbose_name="страница")
    text = models.TextField(blank=True)
    date = models.DateTimeField(auto_now=True)

    is_active = models.BooleanField(default=True)


class Config(models.Model):
    CONFIG_KIND = (
        ('str', 'строка'),
        ('int', 'число'),
    )

    name = models.CharField(max_length=255, unique=True, verbose_name="имя")
    kind = models.CharField(max_length=20, choices=CONFIG_KIND, verbose_name="тип")
    value = models.TextField(blank=True, verbose_name="значение")

