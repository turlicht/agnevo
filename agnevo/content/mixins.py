from django.core.exceptions import ObjectDoesNotExist
from content.models import Content


class ContentMixin():
    content_names = []

    def get_context_data(self, **kwargs):
        context = super(ContentMixin, self).get_context_data(**kwargs)
        context['content'] = {}
        for content_name in self.content_names:
            content, _ = Content.objects.get_or_create(kind=content_name, defaults={'is_active': False})
            context['content'][content_name] = content

        return context
